import React, { Component } from 'react';
import './App.css';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      nCamadas: 0, 
      alturaDaAgua: 0, 
      ocr: 0, 
      cc: 0,
      cr: 0,
      ho: 0,
      eo: 0,
      tpa: 0,
      acrescimoTensao: 0, 
      tensaoInicial: 0, 
      tensaoFinal: 0,
      recalque: 0,
    };
    this.camadasChange = this.camadasChange.bind(this);
    this.acrescimoChange = this.acrescimoChange.bind(this);
    this.aguaChange = this.aguaChange.bind(this);
    this.ocrChange = this.ocrChange.bind(this);
    this.ccChange = this.ccChange.bind(this);
    this.crChange = this.crChange.bind(this);
    this.hoChange = this.hoChange.bind(this);
    this.tpaChange = this.tpaChange.bind(this);
    this.eoChange = this.eoChange.bind(this);
    this.renderPesosEEspessuras= this.renderPesosEEspessuras.bind(this);
    this.renderNumeroDeCamadas = this.renderNumeroDeCamadas.bind(this);
    this.renderAlturaDoNivelDaAgua = this.renderAlturaDoNivelDaAgua.bind(this);
    this.renderAcrescimoDeTensao = this.renderAcrescimoDeTensao.bind(this);
    this.calculaTensao= this.calculaTensao.bind(this);
    this.calculaRecalque = this.calculaRecalque.bind(this);
    this.calcula = this.calcula.bind(this);
    this.renderOCR = this.renderOCR.bind(this);
    this.renderCc = this.renderCc.bind(this);
    this.renderCr = this.renderCr.bind(this);
    this.renderHo = this.renderHo.bind(this);
    this.renderTpa = this.renderTpa.bind(this);
    this.renderEo = this.renderEo.bind(this);
  }

  tpaChange(e){
    this.setState({tpa: Number.parseFloat(e.target.value)})
  }
  ccChange(e){
    this.setState({cc: Number.parseFloat(e.target.value)})
  }
  hoChange(e){
    this.setState({ho: Number.parseFloat(e.target.value)})
  }
  eoChange(e){
    this.setState({eo: Number.parseFloat(e.target.value)})
  }
  crChange(e){
    this.setState({cr: Number.parseFloat(e.target.value)})
  }
  ocrChange(e){
    this.setState({ocr: Number.parseFloat(e.target.value)})
  }
  acrescimoChange(e){
    this.setState({acrescimoTensao: Number.parseFloat(e.target.value)});
  }
  camadasChange(e){
    this.setState({nCamadas: Number.parseFloat(e.target.value)});
  }
  aguaChange(e){
    this.setState({alturaDaAgua: Number.parseFloat(e.target.value)});
  }

  calculaTensao(){
    var sum = 0;
    this.getCamadasArray().forEach(line => {
      var peso = document.getElementById('peso'+line).value;
      var espessura = document.getElementById('espessura'+line).value;
      sum += peso*espessura;
    });
    var tensao = sum - (10*this.state.alturaDaAgua);
    var final = tensao + Number.parseFloat(this.state.acrescimoTensao,10);
    this.setState({tensaoInicial: tensao, tensaoFinal: final });
    return {tensaoInicial: tensao, tensaoFinal: final};
  }

  calcula(){
    var res = this.calculaTensao();
    this.calculaRecalque(res.tensaoInicial, res.tensaoFinal);
  }

  calculaRecalque(tensaoInicial, tensaoFinal){
    let recalque = 0;
    if(Number.parseFloat(this.state.ocr, 10)===1){
      
      recalque = (this.state.ho * (this.state.cc  / (1+Number.parseFloat(this.state.eo)))) * Math.log10(tensaoFinal/tensaoInicial)
    } else if(this.state.ocr<1){
      recalque = (this.state.ho * (this.state.cr/ (1+Number.parseFloat(this.state.eo)))) * Math.log10(tensaoFinal/tensaoInicial)
    
    } else if(this.state.ocr>1){
      if(this.state.tpa < tensaoFinal){
        
        recalque = this.state.ho * (
          (this.state.cr/ (1+Number.parseFloat(this.state.eo))) * Math.log10(this.state.tpa/tensaoInicial)
          +
          (this.state.cc/ (1+Number.parseFloat(this.state.eo))) * Math.log10(tensaoFinal/this.state.tpa)
        );
      
      }else{
        recalque = (this.state.ho * (this.state.cr/ (1+Number.parseFloat(this.state.eo)))) * Math.log10(tensaoFinal/tensaoInicial);
      }
    }
    if(isNaN(recalque)) recalque = 0;
    this.setState({recalque: recalque});    
  }

  getCamadasArray(){
    var array = [];
    for(var i = 1; i<=this.state.nCamadas; i++){
      array.push(i);
    }
    return array;
  }

  renderPesosEEspessuras(){
    var array = this.getCamadasArray();
    return array.map(line => {      
      return (
      <div id={"line"+line} style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
       <div style={{width: '200px', paddingRight:'10px'}} class="input-group input-group-sm mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-sm">Solo</span>
        </div>
        <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
       </div>
       <div style={{width: '200px', paddingRight:'10px'}} class="input-group input-group-sm mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-sm">Peso (Kn/m3)</span>
        </div>
        <input id={'peso'+line} type="number" step="0.1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
       </div>
       <div style={{width: '200px'}} class="input-group input-group-sm mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-sm">Espessura (metros)</span>
        </div>
        <input id={'espessura'+line} type="number" step="0.1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
       </div>
      </div>
      );
    });
  }

  renderNumeroDeCamadas(){
    return (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Número de Camadas que o solo apresenta</span>
          </div>
          <input type="number" value={this.state.nCamadas} onChange={this.camadasChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    );
  }

  renderOCR(){
    return (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">OCR: Razão de Pré-Adensamento</span>
          </div>
          <input type="number" value={this.state.ocr} onChange={this.ocrChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    );
  }

  renderAlturaDoNivelDaAgua(){
    return (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Altura do Nivel da Água</span>
          </div>
          <input type="number" value={this.state.alturaDaAgua} onChange={this.aguaChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    );
  }
  
  renderAcrescimoDeTensao(){
    return (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Acréscimo de Tensão</span>
          </div>
          <input type="number" value={this.state.acrescimoTensao} onChange={this.acrescimoChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    );
  }

  renderHo(){
    return this.state.ocr<=1 ? (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Ho: Espessura da Camada de Adensamento</span>
          </div>
          <input type="number" value={this.state.ho} onChange={this.hoChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    ) : null;
  }

  renderEo(){
    return (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Eo: Índice de Vazios Iniciais</span>
          </div>
          <input type="number" value={this.state.eo} onChange={this.eoChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    );
  }

  renderCc(){
    return this.state.ocr>=1 ? (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Cc: Índice de Compressibilidade</span>
          </div>
          <input type="number" value={this.state.cc} onChange={this.ccChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    ) : null;
  }

  renderCr(){
    return this.state.ocr<1 || this.state.ocr>1 ? (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Cr: Índice de Recompressibilidade</span>
          </div>
          <input type="number" value={this.state.cr} onChange={this.crChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>  
    ) : null;
  }

  renderTpa(){
    return this.state.ocr>1 ? (
      <div style={{display:'flex', flexDirection: 'row', justifyContent: 'center'}}>
        <div style={{width: '400px'}} class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Tpa: Tensão Pré-Adensamento</span>
          </div>
          <input type="number" value={this.state.tpa} onChange={this.tpaChange} step="1" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
        </div>
      </div>
    ) : null;
  } 

  render() {
    return ( 
      <div className="App">

       <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4">Calculadora</h1>
          
          {this.renderNumeroDeCamadas()}
          {this.renderPesosEEspessuras()}
          {this.renderAlturaDoNivelDaAgua()}
          {this.renderAcrescimoDeTensao()}

          {this.renderOCR()}
          {this.renderCc()}
          {this.renderHo()}
          {this.renderEo()}
          {this.renderCr()}
          {this.renderTpa()}

          <div style={{display:'flex',flexDirection: 'row', justifyContent:'center'}}>
             <button style={{width: '200px'}} type="button" onClick={this.calcula}class="btn btn-primary btn-lg btn-block">Calcular</button>
          </div>
          
          <div style={{display: 'flex', flexDirection: 'column', justifyContent:'center'}}>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent:'center'}}>  
            <h3 style={{color: 'black'}}>Recalque
              <span style={{margin: '30px'}}class="badge badge-secondary">{this.state.recalque.toFixed(4)}</span>
            </h3>
            <h3 style={{color: 'black'}}>Tensão Efetiva Inicial
              <span style={{margin: '30px'}}class="badge badge-secondary">{this.state.tensaoInicial.toFixed(4)}</span>
            </h3>
            <h3 style={{color: 'black'}}>Tensão Efetiva Final
              <span style={{margin: '30px'}}class="badge badge-secondary">{this.state.tensaoFinal.toFixed(4)}</span>
            </h3>
          </div>
          </div>
          
        </div>
       </div>
      
      <div style={{marginBottom:'20px',color:'white'}}>
        @Copyright Este software foi desenvolvido para Dianne Guerson para o curso de Engenharia Civil.
      </div>

      </div>
    );
  }
}

export default App;
